let stone_btn=document.querySelector(".stone");
let paper_btn=document.querySelector(".paper");
let siccor_btn=document.querySelector(".siccor");
let Status_heading=document.querySelector(".R-status-heading");
let Score_board_user=document.querySelector('.user_score');
let Score_board_comp=document.querySelector('.Comp_score');
let Reset_btn=document.querySelector('.Reset-btn');
let Check_winner_btn=document.querySelector('.Check_winner-btn');
let winner_choose_text=document.querySelector('.winner_winner');
let winner=document.querySelector('.winner');
let main_flow=document.querySelector('.main');
let new_game_btn=document.querySelector('.Newgame');


let comp_scr=0;
let user_scr=0;


let comp_choice=0;
let user_choice=0;
let sps_arr=['Stone','paper','Scissor'];





stone_btn.addEventListener('click',()=>{
   user_choice=1; 
   comp_choice=Math.ceil(3*Math.random(1,3));
    if((comp_choice === 1 && user_choice === 2)||(comp_choice === 2 && user_choice === 3)||(comp_choice === 3 && user_choice === 1))
    {
        user_scr++;
        // Score_board.innerText=`${user_scr}                ${comp_scr}`;
        Score_board_user.innerText=user_scr;
        Status_heading.innerText=`${sps_arr[user_choice-1]} Nailed, Comp's ${sps_arr[comp_choice-1]}`;
        Status_heading.style.backgroundColor='green';
        Status_heading.style.color='black';
        console.log(`User_choice : ${sps_arr[user_choice-1]} Computer_choice : ${sps_arr[comp_choice-1]}`);
        console.log("Yes,You win");
    }
    else if((user_choice === 1 && comp_choice === 2)||(user_choice === 2 && comp_choice === 3)||(user_choice === 3 && comp_choice === 1)){
        comp_scr++;
        // Score_board.innerText=`${user_scr}                ${comp_scr}`;
        Score_board_comp.innerText=comp_scr;
        Status_heading.innerText=`${sps_arr[user_choice-1]} is Nailed By, Comp's ${sps_arr[comp_choice-1]}`;
        Status_heading.style.backgroundColor='red';
        Status_heading.style.color='black';
        console.log(`User_choice : ${sps_arr[user_choice-1]} Computer_choice : ${sps_arr[comp_choice-1]}`);
        console.log("Aree!You Lose");
    }
    else{
        Status_heading.innerText="It's a Tie";
        Status_heading.style.backgroundColor='black';
        Status_heading.style.color='white';
        Status_heading.style.border='3px solid rgb(134, 182, 246)';
        console.log(`User_choice : ${sps_arr[user_choice-1]} Computer_choice : ${sps_arr[comp_choice-1]}`);
        console.log("Its a tie");
    }
});


paper_btn.addEventListener('click',()=>{
        user_choice=2;
        comp_choice=Math.ceil(3*Math.random(1,3));
        if((comp_choice === 1 && user_choice === 2)||(comp_choice === 2 && user_choice === 3)||(comp_choice === 3 && user_choice === 1))
        {
            user_scr++;
            // Score_board.innerText=`${user_scr}                ${comp_scr}`;
            Score_board_user.innerText=user_scr;
            Status_heading.innerText=`${sps_arr[user_choice-1]} Nailed, Comp's ${sps_arr[comp_choice-1]}`;
            Status_heading.style.backgroundColor='green';
            Status_heading.style.color='black';
            console.log(`User_choice : ${sps_arr[user_choice-1]} Computer_choice : ${sps_arr[comp_choice-1]}`)
            console.log("Yes,You win");
        }
        else if((user_choice === 1 && comp_choice === 2)||(user_choice === 2 && comp_choice === 3)||(user_choice === 3 && comp_choice === 1)){
            comp_scr++;
            // Score_board.innerText=`${user_scr}                ${comp_scr}`;
            Score_board_comp.innerText=comp_scr;
            Status_heading.innerText=`${sps_arr[user_choice-1]} is Nailed By, Comp's ${sps_arr[comp_choice-1]}`;
            Status_heading.style.backgroundColor='red';
            Status_heading.style.color='black';
            console.log(`User_choice : ${sps_arr[user_choice-1]} Computer_choice : ${sps_arr[comp_choice-1]}`);
            console.log("Aree!You Lose");
        }
        else{
            Status_heading.innerText="It's a Tie";
            Status_heading.style.backgroundColor='black';
            Status_heading.style.color='white';
            Status_heading.style.border='3px solid rgb(134, 182, 246)';
            console.log(`User_choice : ${sps_arr[user_choice-1]} Computer_choice : ${sps_arr[comp_choice-1]}`);
            console.log("Its a tie");
        }
});


siccor_btn.addEventListener('click',()=>{
       user_choice=3;
       comp_choice=Math.ceil(3*Math.random(1,3));
       if((comp_choice === 1 && user_choice === 2)||(comp_choice === 2 && user_choice === 3)||(comp_choice === 3 && user_choice === 1))
       {
           user_scr++;
           Score_board_user.innerText=user_scr;
        //    Score_board.innerText=`${user_scr}                ${comp_scr}`;
           Status_heading.innerText=`${sps_arr[user_choice-1]} Nailed, Comp's ${sps_arr[comp_choice-1]}`;
           Status_heading.style.backgroundColor='green';
           Status_heading.style.color='black';
           console.log(`User_choice : ${sps_arr[user_choice-1]} Computer_choice : ${sps_arr[comp_choice-1]}`)
           console.log("Yes,You win");

       }
       else if((user_choice === 1 && comp_choice === 2)||(user_choice === 2 && comp_choice === 3)||(user_choice === 3 && comp_choice === 1)){
        comp_scr++;
        // Score_board.innerText=`${user_scr}                ${comp_scr}`;
        Score_board_comp.innerText=comp_scr;
        Status_heading.innerText=`${sps_arr[user_choice-1]} is Nailed By, Comp's ${sps_arr[comp_choice-1]}`;
        Status_heading.style.backgroundColor='red';
        Status_heading.style.color='black';
        console.log(`User_choice : ${sps_arr[user_choice-1]} Computer_choice : ${sps_arr[comp_choice-1]}`);
        console.log("Aree!You Lose");
       }
       else{
        Status_heading.innerText="It's a Tie";
        Status_heading.style.backgroundColor='black';
        Status_heading.style.color='white';
        Status_heading.style.border='3px solid rgb(134, 182, 246)';
        console.log(`User_choice : ${sps_arr[user_choice-1]} Computer_choice : ${sps_arr[comp_choice-1]}`);
        console.log("Its a tie");
       }
});


// Reset button logic

Reset_btn.addEventListener('click',()=>{

    reset_fun();
});

//reset function

const reset_fun=()=>{
    if(user_scr===0 && comp_scr===0){
        alert("Firtly Start the Game, Then reset it");
        return;
    }
    Score_board_comp.innerText='0';
    Score_board_user.innerText='0';
    user_scr=0;
    comp_scr=0
    Status_heading.style.backgroundColor='rgb(193, 236, 228)';
    Status_heading.style.border='3px solid black';
    Status_heading.innerText='Pick Your Move';
    Status_heading.style.color='black'
};




// winner choiceing logic
Check_winner_btn.addEventListener('click',()=>{
    if(user_scr===0 && comp_scr===0){
        alert("Firtly Start the Game, Then check winner");
        return;
    }
    main_flow.style.display='none';
    winner.style.display='flex';
    check_who_is_winner();

});


//new game btn logic

new_game_btn.addEventListener('click',()=>{
    main_flow.style.display='block';
    winner.style.display='none';
    reset_fun();

});


const check_who_is_winner=()=>{
    if(user_scr<comp_scr){
        winner_choose_text.innerText='Sorry, You Lose!'
    }
    else if(user_scr>comp_scr)
    {
        winner_choose_text.innerText='Congrts, You Won!'
    }
    else{
        winner_choose_text.innerText='Its a Tie..'
    }
};
